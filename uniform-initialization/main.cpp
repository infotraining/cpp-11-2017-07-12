#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;
using namespace Catch::Matchers;

struct Point
{
    int x, y;
};

struct Data
{
    int x;
    int data[3];
    double value;
};

double get_value()
{
    static double counter;

    return ++counter;
}

class Vector2D
{
    int x_, y_;
public:
    Vector2D(int x = 0, int y = 0) : x_{x}, y_{y}
    {}

    int x() const
    {
        return x_;
    }

    int y() const
    {
        return y_;
    }

    static Vector2D versor_x()
    {
        return {1, 0};
    }
};

class Container
{
    vector<int> data_;
public:
    using iterator = vector<int>::iterator;
    using const_iterator = vector<int>::const_iterator;

    Container(initializer_list<int> il) : data_{il}
    {
    }

    iterator begin()
    {
        return data_.begin();
    }

    iterator end()
    {
        return data_.end();
    }

    const_iterator begin() const
    {
        return data_.begin();
    }

    const_iterator end() const
    {
        return data_.end();
    }

    const_iterator cbegin()
    {
        return data_.begin();
    }

    const_iterator cend()
    {
        return data_.end();
    }

    const_iterator cbegin() const
    {
        return data_.begin();
    }

    const_iterator cend() const
    {
        return data_.end();
    }

    size_t size() const
    {
        return data_.size();
    }
};

TEST_CASE("uniform initialization")
{
    SECTION("for simple types")
    {
        int x{10};
        REQUIRE(x == 10);

        int y = {10};
        REQUIRE(y == 10);

        int z{};
        REQUIRE(z == 0);

        bool flag{};
        REQUIRE(flag == false);

        int* ptr{};
        REQUIRE(ptr == nullptr);
    }

    SECTION("for aggregates")
    {
        int tab[10] = { 1, 2, 3 };
        Point pt1{0, 1};

        Data d{ 1, {1, 2}, 4.3}; // { 1, [1, 2, 0], 4.3 }
    }

    SECTION("narrowing conversion is an error")
    {
        double x{get_value()};
    }

    SECTION("for user classes")
    {
        Vector2D vec1{0, 10};
        REQUIRE(vec1.x() == 0);
        REQUIRE(vec1.y() == 10);
    }

    SECTION("for containers")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 };

        map<int, string> dict = { {1, "one"}, {2, "two"}, {3, "three"} };
        REQUIRE(dict[1] == "one");
    }
}

TEST_CASE("initializter lists")
{
    Container cont1 = { 1, 2, 3, 4 }; // calling Container(std::initializer_list<int>)

    for(const auto& item : cont1)
    {
        cout << item << " ";
    }
    cout << endl;

    for(auto it = cont1.cbegin(); it != cont1.cend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    SECTION("constructors with il are prefered")
    {
        vector<int> vec1(10, 1);  // ctor vector(size_t, int)
        REQUIRE_THAT(vec1, Equals(vector<int>{1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }));

        vector<int> vec2{10, 1}; // ctor vector(initializer_list<int>)
        REQUIRE_THAT(vec2, Equals(vector<int>{10, 1}));
    }

    SECTION("with auto")
    {
        auto il = { 1, 2, 3 };
        static_assert(is_same<decltype(il), initializer_list<int>>::value, "Error");

        SECTION("C++17 bug fix")
        {
            auto value1{1};
            auto value2 = {1};
            static_assert(is_same<decltype(value1), int>::value, "Error");
            static_assert(is_same<decltype(value2), initializer_list<int>>::value, "Error");

            // C++17
            //static_assert(is_same<decltype(value2), int>::value, "Error");
        }
    }
}
