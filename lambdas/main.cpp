#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <boost/type_index.hpp>

using namespace std;
using namespace Catch::Matchers;

class MagicLambda_23742345278345
{
public:
    void operator()() const
    {
        cout << "hello from lambda" << endl;
    }
};

struct MagicLambdaWithAuto_23786427834638
{
    template <typename T1, typename T2>
    bool operator()(const T1& n1, const T2& n2) const
    { return *n1 < *n2; }
};

template <typename InIt, typename Func>
void m_for_each(InIt start, InIt end, Func f)
{
    for(InIt it = start;  it != end; ++it)
        f(*it);
}

inline void print(int x)
{
    cout << x << " ";
}

TEST_CASE("lambda")
{
    auto simplest_lambda = []{};
    [](){[](){}();}();

    auto l1 = [] { cout << "hello from lambda " << endl; };

    l1();

    SECTION("underhood it works like this")
    {
        auto l1 = MagicLambda_23742345278345{};

        l1();
    }

    SECTION("deduces return type from single return")
    {
        auto multiply = [](int x, int y) { return x * y; };

        auto result = multiply(2, 4);

        REQUIRE(result == 8);

        SECTION("but require -> for many returns")
        {
            auto desc = [](int x) -> string {
                if (x % 2 == 0)
                    return "even";
                return "odd";
            };

            REQUIRE(desc(2) == string("even"));
            REQUIRE(desc(3) == string("odd"));
        }
    }

    SECTION("can be used in-place")
    {
        vector<unique_ptr<string>> words;

        words.push_back(make_unique<string>("Zenon"));
        words.push_back(make_unique<string>("Ola"));
        words.push_back(make_unique<string>("Ala"));
        words.push_back(make_unique<string>("Marek"));
        words.push_back(make_unique<string>("Antek"));

        sort(words.begin(), words.end(), [](const unique_ptr<string>& n1, const unique_ptr<string>& n2) { return *n1 < *n2; });

        SECTION("C++14")
        {
            sort(words.begin(), words.end(), [](const auto& n1, const auto& n2) { return *n1 < *n2; });

            auto la = [](auto x) { cout << typeid(decltype(x)).name() << endl; };
            la(4);
            la("text");
            la(string("text"));

        }

        for(const auto& w : words)
        {
            cout << *w << " ";
        }
        cout << endl;

        m_for_each(words.begin(), words.end(), [](const unique_ptr<string>& w) { cout << *w << " ";});
        cout << endl;
    }
}

TEST_CASE("STL efficiency")
{
    vector<int> vec = { 1, 2, 3 };

    m_for_each(vec.begin(), vec.end(), &print);
    cout << endl;

    m_for_each(vec.begin(), vec.end(), [](int x) { cout << x << " ";});
    cout << endl;
}

class MagicClosure_78356278356278
{
    const int x_;
public:
    MagicClosure_78356278356278(int x) : x_{x}
    {}

    int operator()() const
    {
        cout << "x from lambda: " << x_ << endl; return x_;
    }
};

class MagicClosure_78356423424278
{
    int& x_;
public:
    MagicClosure_78356423424278(int& x) : x_{x}
    {}

    int operator()() const
    {
        cout << "x from lambda: " << x_ << endl; return ++x_;
    }
};

TEST_CASE("captures and closures")
{
    SECTION("capture by value")
    {
        int x = 42;

        auto l = [x] { cout << "x from lambda: " << x << endl; return x; };

        x = 46;

        REQUIRE(l() == 42);
    }

    SECTION("capture by ref")
    {
        int x = 42;

        auto l = [&x] { cout << "x from lambda: " << x << endl; return ++x; };

        x = 46;

        REQUIRE(l() == 47);
    }

    SECTION("both by value & ref")
    {
        int counter = 0;
        int x = 42;

        auto l = [x, &counter] { counter++; return x; };
        x = 665;

        REQUIRE(l() == 42);
        REQUIRE(counter == 1);
    }
}

// C++14
auto create_generator(int seed)
{
    return [seed]() mutable { return ++seed; };
}

TEST_CASE("mutable lambda")
{
    auto gen = create_generator(665);

    REQUIRE(gen() == 666);
    REQUIRE(gen() == 667);
    REQUIRE(gen() == 668);
}

TEST_CASE("storing lambda")
{
    SECTION("using auto - prefered")
    {
        auto l = [] { cout << "lambda\n"; };
        l();
    }

    SECTION("using ptr to function - when lambda has empty capture list")
    {
        void (*ptr_fun)() = [] { cout << "lambda\n"; };

        ptr_fun();
    }

    SECTION("using std::function")
    {
        int x = 42;
        function<int(int)> f = [x](int y) { return x * y ;};

        REQUIRE(f(10) == 420);
    }
}

class Printer
{
public:
    void operator()(int x) { cout << "Printer prints: " << x << endl; };
};

TEST_CASE("std::function")
{
    function<void(int)> f;

    SECTION("can store function pointer")
    {
        f = &print;
        f(10); // calling a print
    }

    SECTION("can store functor")
    {
        Printer prn;
        f = prn;
        f(11);
    }

    SECTION("can store lambda")
    {
        f = [](int x) { cout << "printing from lambda: " << x << endl; };
        f(12);

        cout << "type of f: " << boost::typeindex::type_id<decltype(f)>().pretty_name() << endl;

        auto af = [](int x) { cout << "printing from lambda: " << x << endl; };

        cout << "type of f: " << boost::typeindex::type_id<decltype(af)>().pretty_name() << endl;
    }
}
