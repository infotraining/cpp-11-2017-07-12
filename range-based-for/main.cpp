#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("range-based-for")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6 };

    SECTION("works with containers")
    {
        for(int item : vec)
        {
            cout << item << " ";
        }
        cout << endl;

        SECTION("is interpreted by compiler as")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                int item = *it;
                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with auto declaration")
    {
        for(auto item : vec)
        {
            cout << item << " ";
        }
        cout << endl;

        SECTION("but we should avoid performance trap")
        {
            vector<string> words = { "one", "two", "three" };

            for(const auto& word : words)
            {
                cout << word << " ";
            }
            cout << endl;

            SECTION("works as well as")
            {
                for_each(words.begin(), words.end(), [](const string& word) { cout << word << " "; });
                cout << endl;
            }
        }
    }

    SECTION("works for native arrays")
    {
        int tab[10] = { 1, 2, 3, 4, 5, 6, 7, 8 };

        for(const auto& item : tab)
        {
            cout << item << " ";
        }
        cout << endl;

        SECTION("is interpreted by compiler as")
        {
            for(auto it = begin(tab); it != end(tab); ++it)
            {
                int item = *it;
                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with initializer lists")
    {
        for(const auto& item : { "one", "two", "three" })
        {
            cout << item << " ";
        }
        cout << endl;
    }
}
