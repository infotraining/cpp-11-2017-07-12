#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget(int id) : Gadget{id, "notset"}
    {
        name_ += "!";
    }

    Gadget(const string& name) : name_{name}
    {}

    Gadget(int id,const string& name) :id_{id}, name_{name}
    {}

    Gadget() = default;

    virtual ~Gadget() = default;

    virtual void info() const
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

namespace Cpp98
{
    class NoCopyable
    {
    public:
        NoCopyable() {}

    private:
        NoCopyable(const NoCopyable&) ;
        NoCopyable& operator=(const NoCopyable&);
    };
}

class NoCopyable
{
protected:
    NoCopyable() = default;

private:
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

class NoCopyableGadget : NoCopyable, public Gadget
{
public:
    using Gadget::Gadget; // inheriting constructors

    NoCopyableGadget(int id) : NoCopyableGadget{id, "unset"}  // reimplementing constructor
    {
    }

    void info() const override final
    {
        cout << "NoCopyableGadget(" << id() << ", " << name() << ")\n";
    }
};

class SuperGadget : public NoCopyableGadget
{
public:
    using NoCopyableGadget::NoCopyableGadget;

//    void info() const override // compilation error
//    {
//    }
};

void foo(int x)
{
    cout << "foo(int: " << x << ")" << endl;
}

void foo(double) = delete;

TEST_CASE("classes with default special function")
{
    Gadget g1{1, "ipad"};
    REQUIRE(g1.id() == 1);
    REQUIRE(g1.name() == "ipad");

    Gadget g2{};
    REQUIRE(g2.id() == -1);
    REQUIRE(g2.name() == "unknown");

    Gadget g3{2};
    REQUIRE(g3.id() == 2);
    REQUIRE(g3.name() == "notset!");
}

TEST_CASE("NoCopyable")
{
    NoCopyableGadget ncg{1, "mp3 player"};
    //auto nc2 = ncg;  // compilation error
}

struct Aggregate
{
    int x;
    int y;
};

struct NonAggregate
{
    int x = 0;
    int y;
};


TEST_CASE("deleted functions")
{
    foo(42);

    short sx = 22;
    foo(sx);

    // foo(3.14);  // compilation error

    // foo(3.14f); // compilation error
}

TEST_CASE("Aggregates")
{
    Aggregate a1{1};
    REQUIRE(a1.y == 0);

//    NonAggregate a2{1};  // compilation error
//    REQUIRE(a2.y == 0);
}

TEST_CASE("overriding virtual functions")
{
    NoCopyableGadget ncg{8, "mouse"};
    Gadget* g = &ncg;

    g->info();

    SuperGadget sg{9, "super mouse"};
    g = &sg;
    g->info();
}
