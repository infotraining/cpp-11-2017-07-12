#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <chrono>

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("raw-strings")
{
    string raw_str = R"(c:\nasza\teczka)";

    REQUIRE(raw_str == "c:\\nasza\\teczka");
}

TEST_CASE("raw-string can be used with many lines")
{
    string raw_str = R"(Line1
Line2
Line3)";

    REQUIRE(raw_str == "Line1\nLine2\nLine3");
}

TEST_CASE("raw-strings have custom delimiters")
{
    string raw_str = R"ericsson(c:\text)ericsson";

    REQUIRE(raw_str == "c:\\text");
}

TEST_CASE("c++14 has new string literal")
{
    auto txt1 = "text"; // const char*
    auto txt2 = "text"s; //  std::string

    SECTION("literals for chrono in c++14")
    {
        REQUIRE(chrono::milliseconds(100) == 100ms);
        REQUIRE(chrono::seconds(2) == 2s);
    }
}
