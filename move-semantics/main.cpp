#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

string get_full_name(const string& fn, const string& ln)
{
    return fn + " " + ln;
}

class IPerson
{
public:
    virtual ~IPerson() = default;
    virtual void info() const = 0;
};


class Person : public IPerson
{
    string name_;
public:
    Person() = default;

    Person(const string& name) : name_{name}
    {}

    //    virtual ~Person() = default;

    //    Person(const Person&) = default;
    //    Person& operator=(const Person&) = default;

    //    Person(Person&&) noexcept = default;
    //    Person& operator=(Person&&) = default;

    string name() const
    {
        return name_;
    }

    void set_name(const string& name)
    {
        cout << "Person::set_name(const string&: " << name << ")\n";
        name_ = name; // string& operator=(const string&)

    }

    void set_name(string&& name)
    {
        cout << "Person::set_name(string&&: " << name << ")\n";
        name_ = move(name); // string& operator=(string&&)
    }

    void info() const override
    {
        cout << "Person(" << name_ << ")\n";
    }
};

namespace WithLegacyCode
{
    class Workgroup
    {
        Person* stuff_ = nullptr;
        size_t size_ = 0;
        string name_ = "unknown";
    public:
        Workgroup() = default;

        Workgroup(initializer_list<Person> il) : stuff_{new Person[il.size()]}, size_{il.size()}
        {
            copy(il.begin(), il.end(), stuff_);

            cout << "Workgroup(il: " << il.size() << ")\n";
        }

        Workgroup(const Workgroup& wg) : stuff_{new Person[wg.size_]}, size_{wg.size_}, name_{wg.name_}
        {
            copy(wg.stuff_, wg.stuff_ + size_, stuff_);

            cout << "Workgroup(cc: " << size_ << ")\n";
        }

        void swap(Workgroup& other)
        {
            std::swap(stuff_, other.stuff_);
            std::swap(size_, other.size_);
            name_.swap(other.name_);
        }

        Workgroup& operator=(const Workgroup& wg)
        {
            Workgroup temp(wg);
            swap(temp);

            cout << "Workgroup op=(cc: " << size_ << ")\n";

            return *this;
        }

        // move constructor
        Workgroup(Workgroup&& wg) noexcept: stuff_{move(wg.stuff_)}, size_{move(wg.size_)}, name_{move(wg.name_)}
        {
            wg.stuff_ = nullptr;
            wg.size_ = 0;

            cout << "Workgroup(mv: " << size_ << ")\n";
        }

        // move assignment
        Workgroup& operator=(Workgroup&& wg)
        {
            if (this != &wg)
            {
                stuff_ = move(wg.stuff_);
                size_ = move(wg.size_);
                name_ = move(wg.name_);

                wg.stuff_ = nullptr;
                wg.size_ = 0;

                cout << "Workgroup op=(mv: " << size_ << ")\n";
            }

            return *this;
        }

        ~Workgroup()
        {
            cout << "~Workgroup(size: " << size_ << ")" << endl;
            delete[] stuff_;
        }

        Person& operator[](size_t index)
        {
            if (size_ == 0 || index >= size_)
                throw out_of_range("index out of range");
            return stuff_[index];
        }

        const Person& operator[](size_t index) const
        {
            if (size_ == 0 || index >= size_)
                throw out_of_range("index out of range");

            return stuff_[index];
        }
    };
}

class Workgroup
{
    vector<Person> stuff_;
    string name_ = "unknown";
public:
    Workgroup() = default;

    Workgroup(initializer_list<Person> il) : stuff_{il}
    {
        cout << "Workgroup(il: " << il.size() << ")\n";
    }

    void swap(Workgroup& other)
    {
        std::swap(stuff_, other.stuff_);
        name_.swap(other.name_);
    }

    Person& operator[](size_t index)
    {
        return stuff_.at(index);
    }

    const Person& operator[](size_t index) const
    {
        return stuff_.at(index);
    }
};


Workgroup create_workgroup()
{
    Workgroup wg{ Person{"1"}, Person{"2"} };

    return wg;
}

class Company
{
    vector<Workgroup> workgroups;

public:
    using iterator = vector<Workgroup>::iterator;
    using const_iterator = vector<Workgroup>::const_iterator;

    void add(const Workgroup& wg)
    {
        workgroups.push_back(wg);
    }

    void add(Workgroup&& wg)
    {
        workgroups.push_back(move(wg));
    }

    iterator begin()
    {
        return workgroups.begin();
    }

    iterator end()
    {
        return workgroups.end();
    }

    const_iterator begin() const
    {
        return workgroups.begin();
    }

    const_iterator end() const
    {
        return workgroups.end();
    }

    size_t size() const
    {
        return workgroups.size();
    }
};



TEST_CASE("reference binding")
{
    string first_name = "Jan";

    SECTION("C++98")
    {
        SECTION("l-value can be bound to l-value ref")
        {
            string& lvref = first_name;
        }

        SECTION("r-value can be bound to const l-value ref")
        {
            const string& clvref = get_full_name("Jan", "Kowalski");
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can be bound to r-value ref")
        {
            string&& rvref = get_full_name("Jan", "Kowalski");

            rvref.clear();
            REQUIRE(rvref == "");
        }

        SECTION("l-value can't be bound to r-value ref")
        {
            // string&& rvref = first_name; // compilation error
        }
    }
}

TEST_CASE("moving names")
{
    string first_name = "Jan";
    string last_name = "Kowalski";

    Person p;

    REQUIRE(p.name() == "");

    SECTION("set by copy")
    {
        p.set_name(first_name);
        REQUIRE(p.name() == "Jan");
        REQUIRE(first_name == "Jan");
    }

    SECTION("set with temp")
    {
        p.set_name(get_full_name(first_name, last_name));
        REQUIRE(p.name() == "Jan Kowalski");
    }

    SECTION("set with move")
    {
        p.set_name(move(last_name));
        REQUIRE(p.name() == "Kowalski");

        REQUIRE(last_name == "");
    }
}

TEST_CASE("Workgroup")
{
    SECTION("can be initialized with a list")
    {
        Workgroup wg = { Person{"Jan"}, Person{"Anna"}, Person{"Adam"} };

        REQUIRE(wg[0].name() == "Jan");
    }

    cout << "\n";

    SECTION("can be copied")
    {
        Workgroup wg = { Person{"Jan"}, Person{"Anna"}, Person{"Adam"} };

        Workgroup copy_wg = wg;
    }

    cout << "\n";

    SECTION("can be moved")
    {
        Workgroup wg = { Person{"Jan"}, Person{"Anna"}, Person{"Adam"} };

        Workgroup moved_wg = move(wg);

        REQUIRE(moved_wg[0].name() == "Jan");
        REQUIRE_THROWS_AS(wg[0].name(), out_of_range);
    }

    cout << "\n";

    SECTION("can be stored in vector")
    {
        vector<Workgroup> workgroups;

        workgroups.push_back(Workgroup{ Person{"1"} });
        workgroups.push_back(Workgroup{ Person{"2"}, Person{"3"} });
        workgroups.push_back(Workgroup{ Person{"3"}, Person{"4"}, Person{"5"} });
    }
}


TEST_CASE("Company")
{
    Company ericpol;
    ericpol.add(Workgroup{ Person{"1"} });
    ericpol.add(create_workgroup());

    Company ericsson = move(ericpol);
    REQUIRE(ericsson.size() == 2);
    REQUIRE(ericpol.size() == 0);
}
