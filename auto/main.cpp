#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <type_traits>

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("auto")
{
    int x = 10;
    const int cx = 20;
    int* ptr = &x;
    const int* ptr2const = &cx;

    vector<int> vec = { 1, 2, 3 };
    const vector<int> cvec = { 1, 2, 3 };

    SECTION("can be used with literals")
    {
        auto ax1 = 10; // int
        auto ax2 = 10L; // long
        auto txt1 = "text"; // const char*
        auto txt2 = "text"s; // string
    }

    SECTION("can be used with assignment operator")
    {
        auto ax1 = x; // int
        auto ax2 = cx; // int - const is removed from type specifier

        auto aptr = ptr; // int*
        auto aptr2c = ptr2const; // const int*

        static_assert(is_same<decltype(aptr2c), const int*>::value, "Error");

        auto it1 = vec.begin(); // vector<int>::iterator
        auto it2 = cvec.begin(); // vector<int>::const_iterator
    }

    SECTION("can be used with const, *, & modifiers")
    {
        const auto xc = x; // const int

        const auto cptr = ptr; // int* const
        static_assert(is_same<decltype(cptr), int* const>::value, "Error");

        auto ptr1 = &x; // int*
        auto* ptr2 = &x; // int*
        const auto* acptr = &x; // const int*
        static_assert(is_same<decltype(acptr), const int*>::value, "Error");

        int& refx = x;
        const int& refcx = x;

        auto ax1 = refx; // int
        auto ax2 = refcx; // int

        auto& rx1 = refx; // int&
        auto& rx2 = refcx; // const int&
    }

    SECTION("decay to pointers")
    {
        int tab[10];

        auto atab = tab; // int* - decay
        auto& rtab = tab; // int(&)[10] - no decay
    }
}

TEST_CASE("auto in iteration")
{
    vector<int> vec = { 1, 2, 3 };

    for(auto it = vec.cbegin(); it != vec.cend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
}

struct Vector2D
{
    int x, y;
};

double operator*(const Vector2D& a, const Vector2D& b)
{
    return a.x * b.x + a.y * b.y;
}

template <typename T>
auto multiply(const T& a, const T& b) //-> decltype(a * b)
{
    return a * b;
}

TEST_CASE("auto vs decltype")
{
    vector<int> vec = { 1, 2, 3 };

    auto vec2 = vec;
    REQUIRE_THAT(vec2, Equals(vector<int>{1, 2, 3}));

    decltype(vec) vec3;
    REQUIRE(vec3.size() == 0);

    map<int, string> dict;
    static_assert(is_same<decltype(dict[declval<int>()]), string&>::value, "Error");
    REQUIRE(dict.size() == 0);

    Vector2D v1{1, 0};
    Vector2D v2{1, 1};

    auto scalar_product = multiply(v1, v2);
    REQUIRE(scalar_product == 1.0);
}
