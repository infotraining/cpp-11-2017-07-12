#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <memory>

using namespace std;
using namespace Catch::Matchers;

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget(int id,const string& name) :id_{id}, name_{name}
    {
        cout << "Gadget(" << id << ", " << name_ << ")" << endl;
    }

    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual void info() const
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")\n";
    }

    virtual void use() const
    {
        cout << "using Gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

TEST_CASE("RAII")
{
    mutex mtx;
    vector<int> vec;

    SECTION("unsafe code")
    {
        mtx.lock();

        vec.push_back(1);

        mtx.unlock();
    }

    SECTION("safe code with RAII")
    {
        lock_guard<mutex> lk{mtx}; // mtx.lock()

        vec.push_back(1);
    } // mtx.unlock()
}

namespace Legacy
{
    Gadget* get_gadget(const string& name)
    {
        static int id;
        return new Gadget(++id, name);
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();
    }

    Gadget* create_gadgets()
    {
        Gadget* tab = new Gadget[10];
        return tab;
    }
}

// source function
unique_ptr<Gadget> get_gadget(const string& name)
{
    static int id;
    //return unique_ptr<Gadget>{new Gadget(++id, name)};
    return make_unique<Gadget>(++id, name);
}

void use(Gadget& g)
{
    g.use();
}

// sink function
void use(unique_ptr<Gadget> g)
{
    if (g)
        g->use();
}


TEST_CASE("unique_ptr")
{
    SECTION("controls lifetime of an object")
    {
        //unique_ptr<Gadget> ptrg{ new Gadget{1, "ipad"} }; // C++11
        auto ptrg = make_unique<Gadget>(1, "ipad"); // C++14
        REQUIRE(ptrg->id() == 1);
        REQUIRE((*ptrg).name() == "ipad");
    }

    SECTION("is moveable")
    {
        //unique_ptr<Gadget> ptrg{ new Gadget{2, "ipod"} };
        auto ptrg = make_unique<Gadget>(1, "ipad"); // C++14
        unique_ptr<Gadget> other_ptrg = move(ptrg);
        REQUIRE(ptrg.get() == nullptr);
        REQUIRE(other_ptrg->id() == 1);
    }

    SECTION("is noncopyable")
    {
        unique_ptr<Gadget> ptrg{ new Gadget{2, "ipod"} };
        // unique_ptr<Gadget> other_ptrg = ptrg; // compiler error
    }

    SECTION("unique_ptr can be stored in vector")
    {
        vector<unique_ptr<Gadget>> gadgets;

        unique_ptr<Gadget> superg = make_unique<Gadget>(8, "super");

        gadgets.push_back(get_gadget("ipad"));
        gadgets.push_back(get_gadget("ipod"));
        gadgets.push_back(get_gadget("mp3"));
        gadgets.push_back(get_gadget("mp4"));
        gadgets.push_back(move(superg));

        for(const auto& g : gadgets)
            g->use();

        gadgets.clear();

        cout << "Before end of test" << endl;
    }

    SECTION("unique_ptr<T[]> controls lifetime of arrays")
    {

        SECTION("Legacy code")
        {
            Gadget* tab = new Gadget[10];

            for(size_t i = 0; i < 10; ++i)
                tab[i].use();

            delete[] tab;
        }

        SECTION("C++11")
        {
            unique_ptr<Gadget[]> tab{Legacy::create_gadgets()};

            for(size_t i = 0; i < 10; ++i)
                tab[i].use();
        } // unique_ptr<Gadget[]> deletes all items in tab and frees memory
    }
}

TEST_CASE("Legacy memory management")
{
    Gadget* g = Legacy::get_gadget("ipad");
    g->use();

    Legacy::use(g);
    Legacy::use(Legacy::get_gadget("mp3 player"));  // leak
} // leak

TEST_CASE("Modern C++11/14 memory management")
{
    unique_ptr<Gadget> g = get_gadget("ipad");
    g->use();

    use(*g); // use(Gadget*) - non owning
    use(move(g));
    use(get_gadget("mp3 player")); // use(unique_ptr<Gadget>) - transfer ownership
}

TEST_CASE("shared_ptr")
{
    auto p1 = make_shared<Gadget>(1, "tablet");
    {
        auto p2 = p1;

        REQUIRE(p2.use_count() == 2);
    }

    REQUIRE(p1.use_count() == 1);

    SECTION("unique_ptr can be moved to shared_ptr")
    {
        unique_ptr<Gadget> g1 = make_unique<Gadget>(6, "mobile");

        shared_ptr<Gadget> sg1 = move(g1);
        sg1 = get_gadget("mobile2");
    }
}

TEST_CASE("weak_ptr")
{
    weak_ptr<Gadget> wp;
    auto sp1 = make_shared<Gadget>(1, "ipad");

    {
        auto sp2 = sp1;
        REQUIRE(sp1.use_count() == 2);

        wp = sp2;
        REQUIRE(sp1.use_count() == 2);
    }

    SECTION("must converted to shared_ptr to use object")
    {
        SECTION("using shared_ptr constructor")
        {
            shared_ptr<Gadget> local_sp{wp};
            local_sp->use();
            local_sp.reset();

            SECTION("when object is deallocated constructor throws")
            {
                REQUIRE(sp1.use_count() == 1);
                sp1.reset();

                REQUIRE_THROWS_AS(shared_ptr<Gadget>{wp}, std::bad_weak_ptr);
            }
        }

        SECTION("using lock()")
        {
            shared_ptr<Gadget> local_sp = wp.lock();
            if (local_sp)
                local_sp->use();
        }
    }
}


namespace Legacy
{
    template <typename T, typename Arg1>
    unique_ptr<T> make_unique_1(Arg1&& arg1)
    {
        return std::unique_ptr<T>{ new T(forward<Arg1>(arg1)) };
    }

    template <typename T, typename Arg1, typename Arg2>
    unique_ptr<T> make_unique_2(Arg1&& arg1, Arg2&& arg2)
    {
        return std::unique_ptr<T>{ new T(forward<Arg1>(arg1), forward<Arg2>(arg2)) };
    }

    template <typename T, typename Arg1, typename Arg2, typename Arg3>
    unique_ptr<T> make_unique_3(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3)
    {
        return std::unique_ptr<T>{ new T(forward<Arg1>(arg1), forward<Arg2>(arg2)), forward<Arg3>(arg3) };
    }
}

template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(forward<Args>(args)...));
}

TEST_CASE("make_unique")
{
    unique_ptr<Gadget> g = my_make_unique<Gadget>(1, "mp3 player");

    // -> std::unique_ptr<Gadget>{ new Gadget(1, "mp3 player") };
}



// head-tail with variadic templates

void print()
{
    cout << endl;
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail)
{
    cout << head << " ";
    print(tail...);
}

TEST_CASE("variadic templates")
{
    print(1, 3.14, "test");
    print(1, 3.14, "test", 89, string("txt"));
}
