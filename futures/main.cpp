#include <iostream>
#include <future>
#include <chrono>
#include <functional>
#include <stdexcept>
#include <vector>

using namespace std;

int calculate_square(const string& id, int x)
{
    cout << id << " has started..." << endl;

    this_thread::sleep_for(x * 1000ms);

    return x * x;
}

int main()
{    
    cout << "Cores: " << thread::hardware_concurrency() << endl;

    future<int> f1 = async(launch::async, &calculate_square, "1", 1);
    future<int> f2 = async(launch::async, &calculate_square, "2", 2);
    auto f3 = async(launch::async, [] { return calculate_square("3", 3); });

    vector<future<int>> futures;

    futures.push_back(move(f1));
    futures.push_back(move(f2));
    futures.push_back(move(f3));

    for(int i = 4; i <= 10; ++i)
        futures.push_back(async(launch::async, [i] { return calculate_square(to_string(i), i); }));

    for(auto& result : futures)
        cout << result.get() << " ";
    cout << endl;

}
