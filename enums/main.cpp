#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <type_traits>
#include <unordered_map>

using namespace std;
using namespace Catch::Matchers;

//template <typename T>
//using underlying_type_t = typename underlying_type<T>::type;

TEST_CASE("classic enums can have underlying type")
{
    enum Coffee : u_int8_t { espresso, cappuccino, latte = 44 };

    underlying_type_t<Coffee> value = espresso;

    int int_val = espresso;

    enum Engine { diesel, tdi, hybrid };

    //Engine e = 10;
}

enum class WeekDay { mon = 1, tue, wed, thd, fri, sat, sun };

ostream& operator<<(ostream& out, WeekDay day)
{
    out << static_cast<int>(day) << endl;

    return out;
}

TEST_CASE("scoped enums")
{
    SECTION("implicit conversion from/to int is not allowed")
    {
        WeekDay day = WeekDay::sun;

        int int_val = static_cast<int>(day);
        day = static_cast<WeekDay>(7);

        REQUIRE(day == WeekDay::sun);
    }

    SECTION("ostream with scoped enums")
    {
        WeekDay day = WeekDay::thd;

        cout << "WeekDay: " << day << endl;
    }
}

namespace std
{
    template <>
    struct hash<WeekDay>
    {
        size_t operator()(const WeekDay& day) const
        {
            return std::hash<int>{}(static_cast<int>(day));
        }
    };
}

struct WeekDayHash
{
    size_t operator()(const WeekDay& day) const
    {
        return std::hash<int>{}(static_cast<int>(day));
    }
};

TEST_CASE("scoped enums as keys in hash tables")
{
    unordered_map<WeekDay, string> days_of_week;

    days_of_week.insert(make_pair(WeekDay::mon, "Monday"));

    REQUIRE(days_of_week[WeekDay::mon] == "Monday");
}
