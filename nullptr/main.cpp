#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

void foo(int* ptr)
{
    if (ptr)
        cout << "foo(int*) is using " << *ptr << endl;
    else
        cout << "foo(int*) with null pointer" << endl;
}

void foo(long value)
{
    cout << "foo(int) is using " << value << endl;
}

//void foo(nullptr_t)
//{
//    cout << "foo(nullptr_t)" << endl;
//}

TEST_CASE("nullptr is better NULL")
{
    int* ptr = nullptr;

    REQUIRE(ptr == 0);
    REQUIRE(ptr == nullptr);

    SECTION("is implicitly convertible to pointer type")
    {
        foo(nullptr);
    }
}

int* create_number()
{
    return nullptr;
}

TEST_CASE("nullptr in if")
{
    int* ptr = create_number();

    if (ptr)
    {
        cout << "not nullptr" << endl;
    }

    if (ptr == nullptr)
    {
        cout << "is nullptr" << endl;
    }
}

TEST_CASE("nullptr has it's own type")
{
    try
    {
        throw nullptr;
    }
    catch (nullptr_t)
    {
        cout << "caught nullptr_t" << endl;
    }
}
