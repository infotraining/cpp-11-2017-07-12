#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

constexpr int factorial(int x)
{
    return x == 1 ? 1 : factorial(x-1) * x;
}

TEST_CASE("constexpr")
{
    constexpr int x = 10;
    constexpr int y = 2;

    constexpr auto r = x * y;

    static_assert(r == 20, "Error");

    int tab[factorial(3)] = {};

    for(const auto& item : tab)
        cout << item << " ";
    cout << endl;

    SECTION("can be used in runtime")
    {
        int a = 5;
        cout << factorial(a) << endl;
    }
}
