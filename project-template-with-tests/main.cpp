#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("test")
{
    vector<int> vec = { 1, 2, 3 };

    REQUIRE_THAT(vec, Equals(vector<int>{1, 2, 3}));
}